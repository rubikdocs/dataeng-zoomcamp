WITH trips as (
SELECT
	trip_distance as dist,
    CAST(lpep_pickup_datetime AS DATE) as pickup,
    CAST(lpep_dropoff_datetime AS DATE) as dropoff
FROM
    yellow_taxi_data t
ORDER BY dist DESC)

SELECT * FROM trips
LIMIT 1


-- or

WITH trips as (
SELECT
	trip_distance,
	row_number () over (order by trip_distance DESC) as run_dist,
    CAST(lpep_pickup_datetime AS DATE) as pickup,
    CAST(lpep_dropoff_datetime AS DATE) as dropoff
FROM
    yellow_taxi_data t)

SELECT * FROM trips where run_dist = 1