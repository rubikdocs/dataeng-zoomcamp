with trips as (
select
	passenger_count as pcount,
    cast(lpep_pickup_datetime as DATE) as pickup,
    cast(lpep_dropoff_datetime as DATE) as dropoff
from
    yellow_taxi_data
)

select pcount, count(pcount) as psum from trips t
where t.pickup = '2019-01-01'
and pcount in (2, 3)
group by pcount