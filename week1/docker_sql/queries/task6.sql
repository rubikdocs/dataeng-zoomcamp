with pickup_data as (
	select td.index, tip_amount from yellow_taxi_data td
	join zones z on td."PULocationID" = z."LocationID"
	where z."Zone" = 'Astoria'
),

dropoff_data as (
	select td.index, z."Zone" as d_zone from yellow_taxi_data td
	join zones z on td."DOLocationID" = z."LocationID"
),

ranked_trips as (select dd.d_zone,
	row_number() over (order by tip_amount desc) as runked_tip
	from pickup_data pd join dropoff_data dd
	on pd.index = dd.index
)

select * from ranked_trips where runked_tip = 1